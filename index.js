const fs = require('fs')
const request = require('request')
const progress = require('request-progress')
const cheerio = require('cheerio')
const path = require("path")

const args = process.argv.slice(2)
const downloadUrl = args[0]
const writePath = args[1] ? args[1] : ''

let options = {
    url: downloadUrl,
    encoding: 'utf-8',
    gzip: true,
}
let fileInfo = {}

request(options, (_, res) => {
    if (res.statusCode === 404) throw new Error('Something went wrong! Page not found.')
    const uriParams = res.request.uri.href.split('/').slice(-2)
    fileInfo.id = uriParams[0]
    fileInfo.hash = uriParams[1]
    fileInfo.wt_session = res.headers['set-cookie'][1].split(';')[0]
    const $ = cheerio.load(res.body)
    $('head meta').each((_, elem) => {
        if (elem.attribs.property === 'og:title')
            fileInfo.title = elem.attribs.content
        else if (elem.attribs.name === 'csrf-token')
            fileInfo.csrf = elem.attribs.content
    })
    options = {
        method: 'POST',
        url: `https://wetransfer.com/api/v4/transfers/${fileInfo.id}/download`,
        headers: {
            'x-csrf-token': fileInfo.csrf,
            'content-type': 'application/json;charset=UTF-8',
            'cookie': fileInfo.wt_session
        },
        body: JSON.stringify({ security_hash: fileInfo.hash }),
    };
    request(options, function (_, response) {
        fileInfo.direct_link = JSON.parse(response.body).direct_link
        if (!fileInfo.direct_link) throw new Error('Something went wrong! Link might be incorrect or expired.')
        progress(request(fileInfo.direct_link), {})
            .on('progress', (state) => {
                process.stdout.clearLine();
                process.stdout.cursorTo(0);
                process.stdout.write(`Progress: ${100 * state.percent.toFixed(2)}%`)
            })
            .on('error', (error) => {
                console.log('\nError:', error)
            })
            .on('end', () => {
                console.log('\nDownloaded!')
            })
            .pipe(fs.createWriteStream(path.join(writePath, fileInfo.title)))
    });

})
    .on('error', (error) => {
        console.log('Something went wrong! Link might be incorrect or expired.', error)
    })